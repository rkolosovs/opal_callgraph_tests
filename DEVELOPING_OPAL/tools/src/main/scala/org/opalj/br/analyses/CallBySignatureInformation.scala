/* BSD 2Clause License:
 * Copyright (c) 2009 - 2015
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.br
package analyses

import java.net.URL

/**
 * Prints information about those methods for which we need to do call by signature resolution
 * when we analyze a library.
 *
 * @author Michael Reif
 */
object CallBySignatureInformation extends DefaultOneStepAnalysis {

    override def title: String =
        "computes potential target methods for interface based method calls"

    override def description: String =
        """|Determines for every interface method if there are methods 
           |with matching signatures in classes that are not final and 
           |which do not implement the respective interface. In such cases, and if we 
           |analyze a library, the respective target methods need to be taken into account.""".
            stripMargin('|')

    override def doAnalyze(
        project:       org.opalj.br.analyses.Project[URL],
        parameters:    Seq[String],
        isInterrupted: () ⇒ Boolean
    ): BasicReport = {

        val cbs = project.get(CallBySignatureResolutionKey)

        val methodReferenceStatistics = cbs.methodReferenceStatistics.mkString("\n", "\n", "\n")
        val generalStatistics = cbs.statistics.map(e ⇒ e._1+": "+e._2).mkString("Statistics{\n\t", "\n\t", "\n}")
        BasicReport(methodReferenceStatistics + generalStatistics)
    }

}